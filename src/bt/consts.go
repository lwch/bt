package main

const (
	msgChoke = iota
	msgUnChoke
	msgInterested
	msgUnInterested
	msgHave
	msgBitfield
	msgRequest
	msgPiece
	msgCancel
	msgPort
)

const blockSize = 8192 // block size for download
