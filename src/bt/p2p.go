package main

import (
	"time"

	"git.dev.tencent.com/lwch/torrent"
)

func p2p(t torrent.Torrent, track *tracker) {
	for {
		peers, _ := track.request()
		for _, peer := range peers {
			go func(peer torrent.Peer) {
				c, ok := clients.get(peer.IP, peer.Port, t, track.hash)
				if c != nil && ok {
					c.handle()
				}
			}(peer)
		}
		time.Sleep(time.Second)
	}
}
